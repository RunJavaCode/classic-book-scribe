# BEYOND FEELINGS

## A GUIDE TO CRITICAL THINKING

## PART ONE

### The Context

Anyone who wishes to master an activity must first understand its tools and rules. This is as true of critical thinking as it is of golf, carpentry, flying a plane, or brain surgery. In critical thinking, however, the tools are not material objects but concepts, and the rules govern mental rather than physical performance.

This first section explores seven important concepts--*individuality, critical thinking, truth, knowledge, opinion, evidence,* and *argument*--with a chapter devoted to each. Most of these concepts are so familiar that you may be inclined to wonder whether there is any point to examining them. The answer is yes , for three reasons. First, much of what is commonly believed about these concepts is mistaken. Second, who ever examines them carefully is always rewarded with fresh insights. Third, the more thorough your knowledge of these concepts, the more proficient you will be in your thinking.



任何希望掌握一项活动的人都必须首先了解其工具和规则。批判性思维和高尔夫、木工、驾驶飞机或脑外科一样，都是如此。然而，在批判性思维中，工具不是物质对象，而是概念，规则制约着心理而不是身体的表现。

第一部分探讨了七个重要的概念——*个性、批判性思维、真理、知识、观点、证据* 和*争论*——每一个概念都有一章专门讨论。这些概念中的大多数都很熟悉，以至于你可能倾向于怀疑研究它们是否有任何意义。答案是肯定的，原因有三。首先，人们对这些概念的普遍看法大多是错误的。第二，仔细研究这些概念的人总是能获得新的见解。第三，你对这些概念的了解越透彻，你的思维就会越熟练。

## CHAPTER 1

### Who Are You?

Suppose someone asked,  "Who are you?" It would be simple enough to respond with your name. But if the person wanted to know the entire story about who you are, the question would be more difficult to answer. You'd obviously have to give the details of your height, age, and weight. You'd also have to include all your sentiments and preferences, even the secret ones you've never shared with anyone--your affection for your loved ones; your desire to please the people you associate with; your dislike of your older sister's husband; your allegiance to your favorite beverage, brand of clothing, and music.

假设有人问："你是谁？" 回答你的名字是很简单的。但如果这个人想知道关于你是谁的整个故事，这个问题就比较难回答了。你显然必须提供你的身高、年龄和体重等细节。你还必须包括你所有的情感和喜好，甚至是你从未与任何人分享的秘密——你对你所爱的人的感情；你想取悦与你交往的人；你不喜欢你姐姐的丈夫；你对你最喜欢的饮料、服装品牌和音乐的忠诚度。

Your attitudes couldn't be overlooked either--your impatience when an issue gets complex, your aversion to certain courses, your fear of high places and dogs and speaking in public. The list would go on. To be complete, it would have to include all your characterisitcs--not only the physical but also the emotional and intellectual.

你的态度也不能被忽视——当一个问题变得复杂时你的不耐烦，你对某些课程的厌恶，你对高处、狗和公开演讲的恐惧。这个列表还可以继续。为了完整，它必须包括你所有的性格——不仅是身体上的，还有情感和智力上的。

To provide all that information would be quite a chore. But suppose the questioner was still curious and asked, "How did you get the way you are?" If you patience were not yet exhausted, chances are you'd answer something like this: "I'm this way because I choose to be, because I've considered other sentiments and preferences and attitudes and have made my selections.  The ones I have chosen fit my style and personality best." That answer is natural enough, and in part it's true. But in a larger sense, it's not ture. The impact of the world on all of us is much greater than most of us realize.

要提供所有这些信息将是一件相当麻烦的事情。但是，假设提问者仍然很好奇，并问："你是怎么变成这样的？" 如果你的耐心还没有耗尽，你有可能会这样回答："我之所以是这样的，是因为我选择了这样的方式，因为我考虑了其他的情感、喜好和态度，并作出了选择。 我选择的那些最适合我的风格和个性。" 这个答案足够自然，而且在一定程度上是真实的。但在更大的意义上，它不是真实的。世界对我们所有人的影响要比我们大多数人意识到的大得多。

#### The Influence of Time and Place(时间和地点的影响)

Not only are you a member of a particular species, *Homo sapiens*, but you also exist at a particular time in the history of that species and in a particular place on the planet. That time and place are defined by specific circumstances, understandings, beliefs, and customs, all of which limit your experience and influence your thought patterns. If you had lived in America in colonial times, you likely would have had no objection to the practice of barring women from serving on a jury, entering into a legal contract, owning property, or voting. If you had lived in the nineteenth century, you would have had no objection to young children being denied an education and being hired out by their parents to work sixteen hours a day, nor would you have given any thought to the special needs of adolescence. (The concept of adolescence was not invented until 1904.)

你不仅是一个特定物种——*智人*——的成员，而且你还存在于该物种历史上的一个特定时间和地球上的一个特定地点。那个时间和地点是由特定的环境、理解、信仰和习俗界定的，所有这些都限制了你的经验并影响你的思维模式。如果你生活在美国的殖民时代，你可能不会反对禁止妇女担任陪审员、签订法律合同、拥有财产或投票的做法。如果你生活在19世纪，你就不会反对幼儿被剥夺教育机会，被父母雇用每天工作16个小时，你也不会考虑青春期的特殊需要。(青春期的概念直到1904年才被发明。）

If you had been raised in the Middle East, you would stand much closer to people you converse with than you do in America. If you had been raised in India, you might be perfectly comfortable having your parents choose your spouse for you. If your native language were Spanish and your knowledge of English modest, you probably would be confused by some English colloquialisms. James Henslin offers two amusing examples of such confusion: Chevrolet Novas initially sold very pooly in Mexico because *no va* in Spanish means "it doesn't work"; and Perdue chickens were regarded with a certain suspicion(or worse) because the company's slogan--"It takes a tough man to make a tender chicken"--became in Spanish "It takes an aroused man to make a chicken affectionate."

如果你在中东长大的，你与你交谈的人会比在美国站得更近。如果你是在印度长大的，你可能非常愿意让你的父母为你选择配偶。如果你的母语是西班牙语，而你的英语知识不多，你可能会对一些英语口语感到困惑。詹姆斯-亨斯林提供了两个关于这种混淆的有趣的例子：雪佛兰Novas最初在墨西哥卖得很差，因为no va在西班牙语中的意思是 "不能用"；人们对Perdue鸡也有一定的怀疑（甚至更糟），因为该公司的口号——“硬汉才能做出嫩鸡”——在西班牙语中变成了“只有性欲旺盛的人才能使鸡变得温情”。

People who grow up in Europe, Asiz, or South America have very different ideas of punctuality. As Daniel Goleman explains, "Five minutes is late but permissible for a business appointment in the U.S., but thirty minutes is normal in Arab countries.  In England five to fifteen minutes is the 'correct' lateness for one invited to dinner; an Italian might come two hours late, an Ethiopian still later, a Javanese not at all, having accepted only to prevent his host's losing face." A fifferent ethnic origin would also mean different tastes in food. Instead of craving a New York Strip steak and french fries, you might crave "raw monkey brains" or "camel's milk cheese patties cured in dry camel's dung" and washed down with "warm camel's blood." Sociologist Ian Robertson summed up the range of global dietary differences succinctly: "Americans eat oysters but not snails. The french eat snails but not locusts. The zulus eat locusts but not fish. The Jews eat fish but not pork. The Hindus eat pork but not beef. The Russians eat beef but not snakes. The Chinese eat snakes but not not people. The Jale of New Guinea find people delicious."[Note:The reference to Hindus is mistaken.]

在欧洲、亚洲或南美洲长大的人对守时的想法截然不同。正如丹尼尔-戈尔曼所解释的，"在美国，商务约会可以迟到 5 分钟，但在阿拉伯国家，30分钟是正常的。 被邀请参加晚宴的人迟到 5 到 15 分钟是“正确的”；一个意大利人可能会迟到两个小时，一个埃塞俄比亚人可能会更晚，一个爪哇人可能根本不会，他接受邀请只是为了防止他的主人丢脸"。不同的民族起源也意味着对食物的不同口味。与其渴望吃纽约长条牛排和炸薯条，你可能会渴望 "生猴脑 "或 "用干骆驼粪便腌制的骆驼奶奶酪饼"，并用 “热骆驼血 ”来冲淡。社会学家伊恩-罗伯逊简洁地总结了全球饮食差异的范围："美国人吃牡蛎，但不吃蜗牛。法国人吃蜗牛，但不吃蝗虫。祖鲁人吃蝗虫，但不吃鱼。犹太人吃鱼，但不吃猪肉。印度教徒吃猪肉，但不吃牛肉。俄罗斯人吃牛肉，但不吃蛇。中国人吃蛇，但不吃人。新几内亚的Jale人觉得人很好吃。"[注：对印度人的提及是错误的。]

To sum up, living in a different age or culture would make you a different person. Even if you rebelled against the values of your time and place, they still would represent the context of your life--in other words, they still would influence your responses.

总而言之，生活在不同的时代或文化会使你成为一个不同的人。即使你反抗你的时代和地方的价值观，它们仍然会代表你的生活背景--换句话说，它们仍然会影响你的反应。

#### The Influence of ideas(思想的影响)

When one idea is expressed, closely related ideas are simultaneously conveyed, logically and inescapably. In logic, this kinship is expressed by the term ***sequitur***, Latin for "it follows."(The converse is *non sequitur,* "it does not follow.")

当一个想法被表达出来时，密切相关的想法同时被传达出来，在逻辑上是不可避免的。在逻辑学中，这种亲缘关系用*sequitur*一词来表达，拉丁文是 "it follows"（反之为*non sequitur*，"it does not follow"）。

Consider, for example, the idea that many teachers and parents express to young children as a way of encouraging them:"If you believe in yourself, you can succeed at anything." From this it follows that *nothing else* but belief--neither talent nor hard work--is necessary for success. The reason the two ideas are equivalent is that their meanings are inseparably linked.

例如，考虑到许多教师和家长向幼儿表达的想法，作为鼓励他们的一种方式："如果你相信自己，你可以在任何事情上取得成功。" 由此可见，除了信念之外，没有其他东西--无论是天赋还是努力工作--是成功的必要条件。这两个概念之所以等同，是因为它们的含义是密不可分的。

In addition to conveying ideas closely linked to it in meaning, an idea can *imply* other ideas.For example, the idea that there is no real difference between virtue and vice implies that people should not feel bound by common moral standards. Samuel Johnson had this implication in mind when he said:"But if he does really think that there is no distinction between virtue and vice, why, Sir, when he leaves our houses let us count our spoons."

除了传达在意义上与其密切相关的想法之外，一个想法还可以暗示其他想法。例如，美德与恶习之间没有真正区别的想法意味着人们不应该受到共同道德标准的束缚。塞缪尔·约翰逊 (Samuel Johnson) 说：“但如果他真的认为美德与罪恶之间没有区别，为什么先生，当他离开我们的房子时，让我们数一数勺子。”

If we were fully aware of the closely linked meanings and implications of the ideas we encounter, we could easily sort out the sound noes from the unsound, the wise from the foolish, and the helpful from the harmful. But we are seldom fully aware. In manys cases, we take ideas at face value and embrace them with little or no thought of their associated meanings and implications. In the course of time, our actions are shaped by those meanings and implications, whether we are aware of them or not.

如果我们完全意识到我们所遇到的思想的密切联系的意义和影响，我们就可以很容易地从不健全的观点中找出健全的观点，从愚蠢的观点中找出明智的观点，从有害的观点中找出有用的观点。但我们很少能完全意识到。在许多情况下，我们只看表面价值，接受它们，很少或根本不考虑它们的相关含义和影响。随着时间的推移，我们的行动被这些意义和影响所左右，无论我们是否意识到它们。

To appreciate the influence of ideas in people's lives, consider the series of events set in motion by an idea that was popular in psychology more than a century ago and whose influence continues to this day--the idea that "intelligence is genetically determined and cannot be increased."

为了体会思想对人们生活的影响，请考虑一个多世纪前在心理学界流行的、影响延续至今的思想所引发的一系列事件--"智力是由基因决定的，无法提高 "的思想。

> That idea led researchers to devise tests that measure intelligence. The most famous(badly flawed) test determined that the average mental age of white American adults was 13 and that, among immigrants, the average Russian's mental age was 11.34; the average Italian's, 11.01; the average Pole's, 10.74; and the average mental age of "Negroes," 10.41.
>
> 这一想法促使研究人员设计出衡量智力的测试。最著名的（有严重缺陷的）测试确定美国白人成年人的平均智力年龄为13岁，在移民中，俄罗斯人的平均智力年龄为11.34岁；意大利人的平均智力年龄为11.01岁；波兰人的平均智力年龄为10.74岁；而 "黑人 "的平均智力年龄为10.41岁。
>
> Educators read the text results and thought, "Attempts to raise student's intelligence are pointless," so they replaced academic curricula with vocational curricula and embraced a methodology that taught students facts but not the process of judgment.
>
> 教育家们读了文本结果后认为，"试图提高学生的智力是毫无意义的"，因此他们用职业课程取代了学术课程，并接受了一种教给学生事实但不教给学生判断过程的方法。
>
> Legislators read the test results and decided "We've got to do something to keep intellectually inferior people from entering the country," so they revised immigration laws to discrimminate against southern and central Europeans.
>
> 立法者阅读了测试结果并决定“我们必须采取措施阻止智力低下的人进入该国”，因此他们修改了移民法以歧视南欧和中欧人。
>
> Eugenicists, who had long been concerned about the welfare of the human species, saw the tests as a grave warning. They thought, "If intelligence cannot be increased, we must find ways of encouraging reproduction among people of higher intelligence and discouraging it among those of lower intelligence."
>
> 长期以来一直关注人类福利的优生学家将这些测试视为一个严重的警告。他们认为，“如果智力不能提高，我们必须想办法鼓励高智商的人繁殖，并阻止低智商的人繁殖。”
>
> The eugenicists' concern inspired a variety of actions. Margaret Sanger's Planned Parenthood urged the lower classes to practice contraception. Others succeeded in legalizing promoted forced sterilization, notably in Virainia. The U.S. Supreme Court upheld the Virginia law with Justice Oliver Wendell Holmes, Jr. declaring, "Three generations of imbeciles are enough." Over the next five decades 7500 women, including "unwed mothers, prostitutes, petty criminals and children with disciplinary problems" were sterilized. In addition, by 1950 over 150000 supposedly "defective" children, many relatively normal, were held against their will in institutions. They "endured isolation, overcrowding, forced labor, and physical abuse including lobotomy, electroshock, and surgical sterilization."
>
> 优生主义者的担忧激发了各种行动。玛格丽特-桑格的计划生育组织敦促下层民众采取避孕措施。其他人则成功地使强制绝育合法化，特别是在弗吉尼亚州。美国最高法院支持弗吉尼亚州的法律，小奥利弗·温德尔·霍姆斯法官宣布：“三代傻瓜就够了。” 在接下来的五十年里，7500名妇女，包括“未婚母亲、妓女、小罪犯和有纪律问题的儿童”被绝育。此外，到1950年，超过15万名被认为是“有缺陷”的儿童，其中许多是相对正常的，被强行关押在收容所。他们“忍受了隔离、过度拥挤、强迫劳动和身体虐待，包括脑叶切除术、电击和手术绝育。”
>
> Meanwhile, business leaders read the test results and decided, "*We need policies to ensure that workers leave their minds at the factory gate and perform their assigned tasks mindlessly.*" So they enacted those policies. Decades later, when Edwards Deming proposed his "quality control" ideas for involving workers in decision making, business leaders remembered those test results and ignored Deming's advice. (In contrast,the Japanese welcomed Deming's ideas; as a result, several of their industries surged ahead of their American competition.)
>
> 与此同时，企业领导人阅读了测试结果，并决定，“我们需要政策来确保工人将思想留在工厂门口，无意识地执行分配给他们的任务。”因此，他们制定了这些政策。几十年后，当爱德华兹·戴明（Edwards Deming）提出让工人参与决策的“质量控制”理念时，商界领袖记住了这些测试结果，却忽视了戴明的建议。（相比之下，日本人欢迎戴明的想法；因此，他们的一些行业在美国竞争中领先。）

These are the most obvious effects of hereditarianism but they are certainly not the only ones. Others include discrimination against racial and ethnic minorities and the often-paternalistic policies of government offered in response. (Some historians also link hereditarianism to the genocide that occured in Nazi Germany.)

这些是遗传主义最明显的影响，但它们肯定不是唯一的影响。其他影响包括对少数种族和民族的歧视，以及政府为应对这些歧视而采取的家长式政策。(一些历史学家还将遗传主义与纳粹德国发生的种族灭绝联系起来）。)

The innumerable ideas you have encountered will affect your beliefs and behavior in similar ways -- somettimes slightly, at other times profoundly. And this can happen even if you have not consciously embraced the ideas.

你所遇到的无数想法会以类似的方式影响你的信仰和行为--有时是轻微的，有时是深远的。即使你没有有意识地接受这些想法，这也可能发生。

#### The Influence of Mass Cultrue

In centries past, family and teachers were the dominant, and sometimes the only, influence on children. Today, however, the influence exerted by mass culture(the broadcast media, newspapers, magazines, Internet and popular music) often is greater.

在过去的几个世纪里，家庭和教师是对儿童的主要影响，有时甚至是唯一的影响。然而，今天，大众文化（广播媒体、报纸、杂志、互联网和流行音乐）所施加的影响往往更大。

By age 18 the average teenager has spent 11000 hours in the classroom and 22000 hours in front of the television set. He or she has had perhaps 13000school lessons yet has watched more than 750000 commercials. By age thirty-five the same person has had fewer han 20000 school lessons yet has watched approximately 45000 hours of television and close to 2 million commercials.

到18岁时，青少年平均已经在教室里呆了11000个小时，在电视机前呆了22000个小时。他或她可能有13000节学校课程，但却看了超过750000个广告。到了35岁，同样的人只上了不到20000节课，却看了大约45000小时的电视和近200万个广告。

What effects does mass culture have on us? To answer, we need only consider the formats and devices commonly used in the media. Modern advertising typically bombards the public with slogans and testimonials by celebrities. This approach is designed to appeal to emotions and create artificial needs for products and services. As result, many people develop the habit of responding emotionally, impulsively, and gullibly to such appeals. They also tend to acquire values very different from those taught in the home and the school. Ads often portray play as more fulfilling than work, self-gratification as more desirable than self-control, and materialism as more meaningful than idealism.

大众文化对我们有什么影响？要回答这个问题，我们只需要考虑媒体中常用的格式和设备。现代广告通常用名人的口号和推荐词轰炸公众。这种方法旨在吸引情感，创造对产品和服务的人为需求。因此，许多人养成了情绪化、冲动化和轻信地回应这些诉求的习惯。他们也倾向于获得与家庭和学校教育截然不同的价值观。广告常常把娱乐描绘成比工作更令人满足，把自我满足描绘成比自我控制更令人向往，把物质主义描绘成比理想主义更有意义。

Television programmers use frequent scene shifts and sensory appeals such as car crashes, violence, and sexual encounters to keep audience interest from diminishing. Then they add frequent commercial interruptions. This author has analyzed the attention shifts that television viewers are subjected to. In a dramatic program, for example, attention shifts might include camera angle changes;  shifts in story line from one set of characters (or subplot) to another, or from a present scene to a past scene (flashback), or to fantasy; and shifts to "newsbreaks," to commercial breaks, from one commercial to another, and back to the program. Also included might be shifts of attention that occur within commercials. I found as many as 78 shifts per hour, excluding the shifts within commercials. The number of shifts within commercials ranged from 6 to 54 and averaged approximately 17 per fifteen-second commercial. The total number of attention shifts came out to over 800 per hour, or over 14 per minute.

电视节目主持人利用频繁的场景转换和感官诉求，如车祸、暴力和性爱，来保持观众的兴趣不减。然后他们再加上频繁的商业中断。这位作者分析了电视观众所承受的注意力转移。例如，在一个戏剧性的节目中，注意力的转移可能包括摄像机角度的变化；故事线从一组人物（或分镜头）转移到另一组人物，或从现在的场景转移到过去的场景（闪回），或转移到幻想；以及转移到 "突发新闻"，转移到商业广告，从一个商业广告到另一个，再回到节目中。此外，还可能包括在广告中发生的注意力转移。我发现每小时有多达78次的转移，不包括广告中的转移。广告中的转移次数从6次到54次不等，平均每15秒的广告大约有17次。注意力转移的总数超过每小时800次，或每分钟超过14次。

This manipulation has prevented many people from developing a mature attention span. They expect the classroom and the workplace to provide the same constant excitement they get from television. That, of course, is an impossible demand, and when it isn's met they call their teachers boring and their work unfulfilling. Because such people seldom have the patience to read books that require them to think, many publishers have replaced serious books with light fare written by celebrities.

这种操纵方式使许多人无法形成成熟的注意力。他们期望课堂和工作场所能够提供与他们从电视中获得的相同的持续兴奋。当然，这是一个不可能的要求，当这个要求没有得到满足时，他们就说老师很无聊，工作不充实。因为这些人很少有耐心阅读需要他们思考的书籍，所以许多出版商用名人写的轻松读物取代严肃的书籍。

Even when writers of serious books do manage to become published authors, they are often directed to give short, dramatic answers during promotional interviews, sometimes at the expense of accuracy. A man who coaches writers for talk shows offered one client this advice:"If I ask you whether the budget deficit is a good thing, or a bad thing, you should not say, 'Well, it stimulates the economy but it passes on a burden.'  You have to say 'It's a great idea!' or 'It's a terrible idea!' It doesn't matter which."(*Translation:*"Don't give a balanced answer. Give on oversimplified one because it will get you noticed.")

即使当严肃书籍的作者确实能够成为出版作家时，他们在接受宣传采访时也经常被要求作出简短、戏剧性的回答，有时还要牺牲准确性。一位指导作家参加谈话节目的人给一位客户提供了这样的建议："如果我问你预算赤字是好事还是坏事，你不应该说，'嗯，它刺激了经济，但它转嫁了一个负担。’ 你必须说'这是一个伟大的想法！'或者'这是一个可怕的想法！' 哪个都不重要。"（译文："不要给出一个平衡的答案。给予过度简化的答案，因为它会让你受到关注。")

Print journalism is also in the grip of sensationalism. As a newspaper editor observed, "Journalists keep trying to find people who are at 1 or at 9 on a scale of 1 to 10 rather than people at 3 to 7[the more moderate positions] where most people actually are." Another journalist claims, "News is now becoming more opinion than verified fact. Journalists are slipping into entertainment rather than telling us the verified facts we need to know."

印刷新闻也在耸人听闻之中。正如一位报纸编辑所观察到的那样，“记者们一直试图在 1 到 10 的范围内找到 1 或 9 的人，而不是大多数人实际所处的 3 到 7[更温和的位置]的人。”另一位记者声称，“新闻现在变得更多的是观点，而不是经过验证的事实。记者们正在滑入娱乐，而不是告诉我们我们需要知道的经过验证的事实。”

Today's politicians often manipulate people more offensively than do journalists. Instead of expressing their thoughts, some politicians find out what people think and pretend to share their ideas. Many politicians hire people to conduct polls and focus groups to learn what messages will "sell." They even go so far as to test the impact of certain words--that is way we hear so much about "trust," "family," "character," and "values" these days. Political science professor Larry Sabato says that during the Clinton impeachment trial, the president's advisors used the term *private lives* over and over--James Carville used it six times in one four-minute speech--because they knew it could persuade people into believing the president's lying uder oath was of no great consequence.

今天的政客们常常比记者们更有进攻性地操纵人们。一些政客不表达自己的想法，而是找出人们的想法，假装与他们的想法一致。许多政客雇人进行民意调查和焦点小组，以了解哪些信息会 "畅销"。他们甚至不惜测试某些词语的影响——这就是我们如今听到这么多关于 "信任"、"家庭"、"性格 "和 "价值观 "的方式。政治学教授拉里-萨巴托（Larry Sabato）说，在克林顿的弹劾审判中，总统的顾问们一次又一次地使用 "私生活 "一词——詹姆斯-卡维尔（James Carville）在一次四分钟的演讲中使用了六次——因为他们知道这可以说服人们相信总统在宣誓前撒谎并没有什么大的影响。

#### The "Science" of Manipulation

Attempts to influence the thoughts and actions of others are no doubt as old as time, but manipulation did not become a science until the early twentieth century, when Ivan Pavlov, a Russian professor of psychology, published his research on conditioned (learned) reflexes. Pavlov found that by ringing a bell when he fed a dog, he could condition the dog to drool at the sound of the bell even when no food was presented. An American psychologist, John Watson, was impressed with Pavlov's findings and applied them to human behavior. In Watson's most famous experiment, he let a baby touch a laboratory rat. At first, the baby was unafraid. But then Watson hit a hammer against metal whenever the baby reached out to touch the rat, and the baby became frightened and cried. In time, the baby cried not only at the sight of the rat but also at the sight of anything furry, such as a stuffed animal. Watson's work earned him the title "father of behaviorism."

试图影响他人的思想和行为的尝试无疑与时间一样古老，但直到 20 世纪初，俄罗斯心理学教授伊万·巴甫洛夫 (Ivan Pavlov) 发表了他对条件反射 (习得) 的研究，操纵才成为一门科学。巴甫洛夫发现，通过在喂狗时按铃声，即使没有食物，他也能让狗在铃声响起时流口水。美国心理学家约翰·华生对巴甫洛夫的发现印象深刻，并将其应用于人类行为。在沃森最著名的实验中，他让一个婴儿触摸一只实验室老鼠。起初，婴儿并不害怕。但后来，每当孩子伸手去碰老鼠时，沃森就用锤子敲击金属，孩子吓得哭了起来。随着时间的推移，婴儿不仅在看到老鼠时哭了，而且在看到任何毛茸茸的东西时也哭了，比如毛绒玩具。沃森的工作为他赢得了“行为主义之父”的称号。

Less well known is Watson's application of behaviorist principles to advertising. He spent the latter part of his career working for advertising agencies and soon recognized that the most effective appeal to consumers was not to the mind but to the emotions. He advised advertisers to "tell[the consumer] something that will tie him up with fear, something that will stir up a mild rage, that will call out an affectionate or love response, or strike at a deep psychological or habit need." His attitude toward the consumer is perhaps best indicated by a statement he made in a presentation to department store executives: "The consumer is to the manufacturer, the department stores and the advertising agencies, what the green frog is to the physiologist."

不太为人所知的是华生在广告中运用了行为主义原则。他在职业生涯的后半段为广告公司工作，很快就意识到对消费者最有效的吸引力不是头脑，而是情感。他建议广告商“告诉（消费者）一些会让他产生恐惧感的事情，一些会激起轻微愤怒的事情，会引起深情或爱的反应，或者是深层次的心理或习惯需求。”他在向百货公司高管做的一次演讲中的一句话或许最能说明他对消费者的态度：“消费者对制造商、百货公司和广告公司来说，就像绿青蛙对生理学家一样。”

Watson introduced these strategies in the 1920s and 1930s, the age of newspapers and radio. Since the advent of television, these advertising strategies have grown more sophisticated and effective, so much so that many individuals and groups with political and social agendas have adopted them. The strategies work for a number of reasons, the chief one being people's conviction that they are impervious to manipulation. This belief is mistaken, as many researchers have demonstrated. For example, Solomon Asch showed that people's reactions can be altered simply by changing the order of words in a series. He asked study participants to evaluate a person by a series of adjectives. When he put positive adjectives first--"intelligent, industrious, impulsive, critical, stubborn, envious"--the participants gave a positive evaluation. When he reversed the order, with "envious" coming first and "intelligent" last, they gave a negative evaluation..

沃森在20世纪20年代和30年代，即报纸和广播的时代，提出了这些策略。自从电视出现以来，这些广告策略变得更加复杂和有效，以至于许多有政治和社会议程的个人和团体都采用了这些策略。这些策略之所以有效，有很多原因，其中最主要的原因是人们相信它们不受操纵。正如许多研究人员所证明的那样，这种观点是错误的。例如，所罗门·阿什（Solomon Asch）指出，人们的反应可以通过改变一系列单词的顺序来改变。他要求研究参与者用一系列形容词来评价一个人。当他把积极的形容词放在第一位——“聪明、勤奋、冲动、挑剔、固执、嫉妒”——参与者给出了积极的评价。当他颠倒顺序，“嫉妒”排在第一，“聪明”排在最后时，他们给出了否定的评价。

Similarly, research has shown that human memory can be manipulated. The way a question is asked can change the details in a person's memory and even make a person *remember something that never happened!*

同样，研究表明，人类的记忆是可以被操纵的。问问题的方式可以改变一个人记忆中的细节，甚至可以让一个人记住从未发生过的事情。

Of course, advertisers and people with political or social agendas are not content to stimulate emotions and /or plant ideas in our minds. They also seek to reinforce those impressions by repeating them again and again. The more people hear a slogan or talking point, the more familiar it becomes. Before long, it becomes indistinguishable from ideas developed through careful thought. Sadly, "the packaging is often done so effectively that the viewer, listener, or reader does not make up his own mind at all. Instead, he inserts a packaged opinion into his mind, somewhat like inserting a DVD into a DVD player. He then pushes a button and 'plays back' the opinion whenever it seems appropriate to do so. He has performed acceptably without having had to think." Many of the beliefs we hold dearest and defend most vigorously may have been planted in our minds in just this way.

当然，广告商和有政治或社会议程的人并不满足于刺激情绪和/或在我们的头脑中植入想法。他们还试图通过一次又一次地重复这些印象来加强这些印象。人们听到的口号或谈话要点越多，就越熟悉。不久之后，它就变得与通过仔细思考而形成的想法没有区别了。可悲的是，"这种包装往往做得如此有效，以至于观众、听众或读者根本就没有做出自己的想法。相反，他把包装好的观点插入他的头脑，有点像把DVD插入DVD播放器。然后，他按下一个按钮，在适当的时候 "回放 "这个观点。他不用思考就已经有了可接受的表现"。许多我们最珍视、最极力捍卫的信念可能就是以这种方式植入我们的头脑中的。

Many years ago, Harry A. Overstreet noted that "a climate of opinion, like a physical climate, is so pervasive a thing that those who live within it and know no other take it for granted." The rise of mass culture and the sophisticated use of manipulation have made this insight more releveant today than ever.

多年前，哈里-A-奥弗斯特里特指出，"舆论氛围就像物理气候一样，是如此普遍，以至于那些生活在其中而不了解其他事物的人认为它是理所当然的。" 大众文化的兴起和对操纵的复杂使用，使这一见解在今天比以往任何时候都更具有现实意义。

#### The Influence of Psychology

The social and psychological theories of our time also have an impact on our beliefs. Before the past few decades, people were urged to be selfdisciplined, self-critical, and self-effacing. They were urged to practice selfdenial, to aspire to self-knowledge, to behave in a manner that ensured they maintained self-respect. Self-centeredness was considered a vice. "Hard Work," they were told, "leads to achievement, and that in turn produces satisfaction and self-confidence." By and large, our grandparents internalized those teachings. When they honored them in their behavior, they felt proud; when they dishonored them, they felt ashamed.

我们这个时代的社会和心理学理论也对我们的信仰产生了影响。在过去几十年前，人们被敦促要自律、自我批判和自我克制。他们被敦促进行自我否定，渴望自我了解，以确保他们保持自尊的方式行事。以自我为中心被认为是一种恶习。他们被告知 "努力工作"，"导致成就，这反过来又会产生满足和自信"。总的来说，我们的祖父母将这些教义内化了。当他们在行为上尊重这些教诲时，他们感到自豪；当他们不尊重这些教诲时，他们感到羞愧。

Today the theories have been changed--indeed, almost exactly reversed. Self-esteem, which nineteenth-century satirist Ambrose Bierce defined as "an erroneous appraisement," is now considered an imperative. Self-centeredness has been transformed from vice into virtue, and people who devote their lives to helping others, people once considered heroic and saintlike, are now said to be afflicted with "a disease to please." The formula for success and happiness begins with feeling good about ourselves. Students who do poorly in school , workers who don't measure up to the challenges of their jobs, substance abusers, lawbreakers--all are typically diagnosed as deficient in self-esteem.

今天，这些理论已经发生了变化--事实上，几乎完全颠倒了。十九世纪的讽刺作家安布罗斯-比尔斯（Ambrose Bierce）将自尊定义为 "一种错误的评价"，现在则被认为是一种必要条件。以自我为中心已经从恶习变成了美德，而那些致力于帮助他人的人，那些曾经被认为是英雄和圣人的人，现在被说成是患了 "取悦的疾病"。成功和幸福的公式始于对自己的良好感觉。在学校表现不好的学生、无法应对工作挑战的工人、药物滥用者、违法者——所有这些人通常被诊断为缺乏自尊。
